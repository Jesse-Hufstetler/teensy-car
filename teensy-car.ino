#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <SD.h>
#include <SerialFlash.h>
#include "Arduino.h"
#include "AudioStream.h"
#include "utility/dspinst.h"
#include <Vector.h>
#include "code.h"

// to get this to compile I had to use arduino ide 1.8.13 and install the vector library, v1.1.2 by peter polidoro.
// arduino 1.8.12 didn't work and it didn't work without the vector thingy
// also you gotta select the right board obvs
