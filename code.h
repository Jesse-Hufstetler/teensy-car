// SUBLIME FOLD WITH CTRL+K, CTRL+1
//#define front_speaks 1
//#define rear_speaks 1
#define woofers 1

const int ledPin = 13;
auto CLIPPING = false;
auto QUIET = false;

#if defined front_speaks || defined rear_speaks
constexpr auto TEST_TONE_FREQ = 250;
#endif

#ifdef woofers
constexpr auto TEST_TONE_FREQ = 25;
#endif

constexpr auto CLIPPING_THRESHOLD = 26000;
constexpr auto BASE_Q = 0.707106781186548;
constexpr auto SQRT2 = 1.414213562373100;

//This limiter has a "look-ahead" section which basically looks ahead
//one cycle of the frequency which you set here. It's kind of like
//clipping distortion but with a low-pass filter on the nasty
//artifacts that clipping creates. That's kind of what it ends up
//sounding like. I call it the "transient catcher" because it swoops
//in and catches transients but in a way which is relatively pleasing
//to the ears.
//
//That means that if you set this number to a really LOW frequency you
//will have a really HIGH look-ahead time and you might get weird
//effects where you can hear the level drop down right before
//something loud happens.
//
constexpr auto TRANSIENT_CATCHER_CUTOFF_FREQUENCY = 500;

//The initial compressor has no concept of "attack time". Instead,
//it attacks instantly but then I put a lowpass filter on the gain
//reduction. That way you know you won't be able to hear artifacts
//of the initial compressor above the frequency you set here. So
//this number kind of serves as the attack time. Hopefully that
//makes sense.
//
constexpr auto SMOOTH_COMPRESSOR_MASK_FILTER_FREQUENCY = 20;

//Kind of self-explanatory, this is the release speed of the
//initial compressor. I noticed you need exponentially bigger
//numbers to get a higher release speed. I used to define it in
//terms of 2 raised to the whatever power and then I would play with
//the exponent rather than the number itself.
//
constexpr auto SMOOTH_COMPRESSOR_RELEASE_SPEED = 128;

class Helpers
{
public:
    static String percentString(const double input)
    {
        return String(input * 100) + "%";
    }
    static double decibelsToGain(const double input)
    {
        return pow(10, input * 0.05);
    }
    static double bandwidthToQ(const double input)
    {
        return pow(2, (input * 0.5)) / (pow(2, input)-1);
    }
};
class TestTone
{
public:
    double phase = 0;
    double getSample(const double sampleRate, const double frequency)
    {
        phase = phase + 2.0 * PI * frequency / sampleRate;
        return sin(phase);
    }
};

class Filter
{
    double a0 = 0;
    double a1 = 0;
    double a2 = 0;
    double b1 = 0;
    double b2 = 0;
public:
    void setHpfCoefficients(const double sampleRate, const double frequency, const double q)
    {
        const auto k = tan(PI * frequency / sampleRate);
        const auto norm = 1 / (1 + k / q + k * k);
        a0 = 1 * norm;
        a1 = -2 * a0;
        a2 = a0;
        b1 = 2 * (k * k - 1) * norm;
        b2 = (1 - k / q + k * k) * norm;
    }
    void setLpfCoefficients(const double sampleRate, const double frequency, const double q)
    {
        const auto k = tan(PI * frequency / sampleRate);
        const auto norm = 1 / (1 + sqrt(2) * k + k * k);
        a0 = k * k * norm;
        a1 = 2 * a0;
        a2 = a0;
        b1 = 2 * (k * k - 1) * norm;
        b2 = (1 - k / q + k * k) * norm;
    }
    void setBandCoefficients(const double sampleRate, const double frequency, const double q, const double gain)
    {
        const auto k = tan(PI * frequency / sampleRate);
        const auto v = pow(10, abs(gain) / 20);
        if (gain >= 0)
        {
            const auto norm = 1 / (1 + 1 / q * k + k * k);
            a0 = (1 + v / q * k + k * k) * norm;
            a1 = 2 * (k * k - 1) * norm;
            a2 = (1 - v / q * k + k * k) * norm;
            b1 = a1;
            b2 = (1 - 1 / q * k + k * k) * norm;
        }
        else
        {
            const auto norm = 1 / (1 + v / q * k + k * k);
            a0 = (1 + 1 / q * k + k * k) * norm;
            a1 = 2 * (k * k - 1) * norm;
            a2 = (1 - 1 / q * k + k * k) * norm;
            b1 = a1;
            b2 = (1 - v / q * k + k * k) * norm;
        }
    }
    void setLowShelfCoefficients(const double sampleRate, const double frequency, const double q, const double gain)
    {
        const auto v = pow(10, abs(gain) / 20);
        const auto k = tan(PI * frequency / sampleRate);
        if (gain >= 0)
        {
            const auto norm = 1 / (1 + SQRT2 * k + k * k);
            a0 = (1 + sqrt(2 * v) * k + v * k * k) * norm;
            a1 = 2 * (v * k * k - 1) * norm;
            a2 = (1 - sqrt(2 * v) * k + v * k * k) * norm;
            b1 = 2 * (k * k - 1) * norm;
            b2 = (1 - SQRT2 * k + k * k) * norm;
        }
        else
        {
            const auto norm = 1 / (1 + sqrt(2 * v) * k + v * k * k);
            a0 = (1 + SQRT2 * k + k * k) * norm;
            a1 = 2 * (k * k - 1) * norm;
            a2 = (1 - SQRT2 * k + k * k) * norm;
            b1 = 2 * (v * k * k - 1) * norm;
            b2 = (1 - sqrt(2 * v) * k + v * k * k) * norm;
        }
    }
    double applyFilter(const double input)
    {
        x0 = input;
        y0 = a0 * x0 + a1 * x1 + a2 * x2
             - b1 * y1 - b2 * y2;
        x2 = x1;
        x1 = x0;
        y2 = y1;
        y1 = y0;
        return y0;
    }
    double x0 = 0;
    double x1 = 0;
    double x2 = 0;
    double y0 = 0;
    double y1 = 0;
    double y2 = 0;
    void setAllTo(const double i)
    {
        x0 = x1 = x2 = y0 = y1 = y2 = i;
    };
};
class PeakLimiter
{
public:
    int lookAheadSamples = 0;
    double threshold = 0;
    double *delayBuffer;
    double *maskBuffer;
    Filter lpf;

    int lookAheadLimiterConstructor(const double frequency, const double sampleRate)
    {
        lookAheadSamples = ceil(sampleRate / TRANSIENT_CATCHER_CUTOFF_FREQUENCY);
        threshold = Helpers::decibelsToGain(-.2);
        delayBuffer = new double[lookAheadSamples];
        maskBuffer = new double[lookAheadSamples];
        lpf.setLpfCoefficients(sampleRate, frequency, BASE_Q);
        return lookAheadSamples;
    }
    double newMask = 0;
    int writePosition = 0;
    int checkLocation = 0;
    int readPosition = 0;
    double filteredMask = 0;
    double lookAheadLimiter(const double input)
    {
        delayBuffer[writePosition] = input;
        if (abs(input) > threshold)
        {
            newMask = threshold / abs(input);
            checkLocation = writePosition;
            maskBuffer[checkLocation] = newMask;
            checkLocation -= 1;
            if (checkLocation < 0)
                checkLocation += lookAheadSamples;
            while (maskBuffer[checkLocation] > newMask)
            {
                maskBuffer[checkLocation] = newMask;
                checkLocation -= 1;
                if (checkLocation < 0)
                    checkLocation += lookAheadSamples;
            }
        }
        else
        {
            maskBuffer[writePosition] = 1;
        }
        filteredMask = lpf.applyFilter(maskBuffer[readPosition]);
        writePosition = (writePosition + 1) % lookAheadSamples;
        readPosition = (writePosition + 1) % lookAheadSamples;
        return delayBuffer[readPosition] * filteredMask;
    }
};
class Booster
{
public:
    double adj = 0;
    double booster(const double input) const
    {
        return adj * input;
    }
    void boosterConstructor(const double dbDifference)
    {
        adj = Helpers::decibelsToGain(dbDifference);
    }
};
class MidSideEncoderDecoder
{
public:
    double temp;
    void midSideEncodeDecode(const double in0, const double in1)
    {
        channel0 = in0;
        channel1 = in1;
        temp = channel0;
        channel0 = (channel0 + channel1) / SQRT2;
        channel1 = (temp - channel1) / SQRT2;
    }
    double channel0;
    double channel1;
};
class Limiter4
{
public:
    Filter lpf;
    double mask = 0;
    double threshold = 0;
    double releaseSpeed = 0;
    void smoothCompressor(const double newReleaseSpeed, const double frequency, const double sampleRate)
    {

        mask = 1;
        threshold = 0;
        releaseSpeed = newReleaseSpeed;
        lpf.setLpfCoefficients(sampleRate, frequency, BASE_Q);
        lpf.setAllTo(1);
    }
    double smoothCompressorSample(double input, const double sampleRate)
    {

        const auto absIn = abs(input);
        if (absIn > threshold)
        {
            mask = min(mask, threshold / absIn);
        }
        const auto filteredMask = lpf.applyFilter(mask);

        input *= filteredMask;
        mask *= exp(SMOOTH_COMPRESSOR_RELEASE_SPEED / sampleRate);
        mask = min(mask, 1.0);
        return input;
    }
};




class Gate
{
public:
    double threshold = 0;
    double samps = 0;


    double gateSample(double input, const double sampleRate)
    {

#if defined front_speaks || defined rear_speaks
        auto fakeSampleRate = 700.0;
#endif

#ifdef woofers
        auto fakeSampleRate = sampleRate;
#endif

        if (abs(input) < threshold)
            samps++;
        else
            samps = 0;
        if (samps > fakeSampleRate)
            if (samps > fakeSampleRate * 2)
                return 0;
            else
                return input * (fakeSampleRate - (samps - fakeSampleRate)) / fakeSampleRate;
        else
            return input;
    }
};




class MonoSampleProcessor
{
public:
    MonoSampleProcessor(const double sampleRate)
    {

        limiter40.smoothCompressor(SMOOTH_COMPRESSOR_RELEASE_SPEED, SMOOTH_COMPRESSOR_MASK_FILTER_FREQUENCY, sampleRate);
        peakLimiter0.lookAheadLimiterConstructor(TRANSIENT_CATCHER_CUTOFF_FREQUENCY, sampleRate);

#if defined front_speaks || defined rear_speaks

#ifdef front_speaks
        inputGain = Helpers::decibelsToGain(26.0);
#endif
#ifdef rear_speaks
        inputGain = Helpers::decibelsToGain(40.0);
#endif
        outThreshold = Helpers::decibelsToGain(-2);
        gate.threshold = Helpers::decibelsToGain(-65);

        highpass.setHpfCoefficients(sampleRate, 200, BASE_Q);
        f1.setBandCoefficients(sampleRate, 280, Helpers::bandwidthToQ(1.084), -18);
        f2.setBandCoefficients(sampleRate, 710, Helpers::bandwidthToQ(1.084), -5);
        f3.setBandCoefficients(sampleRate, 1000, Helpers::bandwidthToQ(1.084), -8);
        f4.setBandCoefficients(sampleRate, 2530, Helpers::bandwidthToQ(1.9), -4);/* NEW */
        f5.setBandCoefficients(sampleRate, 3600, Helpers::bandwidthToQ(1.27), -10);
        f6.setBandCoefficients(sampleRate, 4500, Helpers::bandwidthToQ(0.945), -5);
        f7.setBandCoefficients(sampleRate, 8000, Helpers::bandwidthToQ(0.479), -0);
        f8.setBandCoefficients(sampleRate, 10883, Helpers::bandwidthToQ(1.9), -8);/* NEW */
        f9.setBandCoefficients(sampleRate, 16000, Helpers::bandwidthToQ(0.714), 0);
        lowpass.setLpfCoefficients(sampleRate, 20000, BASE_Q);
#endif

#ifdef woofers
        inputGain = Helpers::decibelsToGain(19);
        outThreshold = Helpers::decibelsToGain(-0.08729610805);
        gate.threshold = Helpers::decibelsToGain(-24);

        highpass.setHpfCoefficients(sampleRate,    10,      BASE_Q);
        f1.setBandCoefficients(sampleRate,         50,    1.3,    -5);
        f2.setBandCoefficients(sampleRate,         23,    2.5,    9);
        f3.setBandCoefficients(sampleRate,         80,    5.0,   7);

        f4.setBandCoefficients(sampleRate,        140,    1.5,   10);
        f5.setBandCoefficients(sampleRate,         55,    3.0,   5);
        f6.setBandCoefficients(sampleRate,        400,    1.0,   10);
        f7.setBandCoefficients(sampleRate,       28,    5, 11);
        f8.setBandCoefficients(sampleRate,       150,    1, 10);
        f9.setBandCoefficients(sampleRate,       165,    15, -10);
        f10.setBandCoefficients(sampleRate,       124,    15, -10);
        f11.setBandCoefficients(sampleRate,       55,    8, -5);
        lowpass.setLpfCoefficients(sampleRate,   300,     BASE_Q); 
#endif

        limiter40.threshold = outThreshold;
        peakLimiter0.threshold = outThreshold;
    }

    double Process(double input, const double sampleRate)
    {
        //input= testToneGenerator.getSample(sampleRate, TEST_TONE_FREQ);
        //input = gate.gateSample(input, sampleRate);

        input = lowpass.applyFilter(input);
        auto defeat = false;
        if (defeat)
        {
            input = input;
        }
        else
        {
            input *= inputGain;
            input = highpass.applyFilter(input);
            input = f1.applyFilter(input);
            input = f2.applyFilter(input);
            input = f3.applyFilter(input);
            input = f4.applyFilter(input);
            input = f5.applyFilter(input);
            input = f6.applyFilter(input);
            input = f7.applyFilter(input);
            input = f8.applyFilter(input);
            input = f9.applyFilter(input);
            input = f10.applyFilter(input);
            input = f11.applyFilter(input);
            if (QUIET) input *= 0.1;
            input = limiter40.smoothCompressorSample(input, sampleRate);
            input = peakLimiter0.lookAheadLimiter(input);
        }

        if (input >= outThreshold) input = outThreshold;
        if (input <= -outThreshold) input = -outThreshold;
        return input;
    }
private:
    Limiter4 limiter40;
    PeakLimiter peakLimiter0;
    Filter highpass;
    Filter lowpass;
    Filter f1;
    Filter f2;
    Filter f3;
    Filter f4;
    Filter f5;
    Filter f6;
    Filter f7;
    Filter f8;
    Filter f9;
    Filter f10;
    Filter f11;
    Gate gate;
    double inputGain;
    double outThreshold;
    TestTone testToneGenerator;
};

class AudioEffectTeensyCar : public AudioStream
{
public:
    AudioEffectTeensyCar(void): AudioStream(2, inputQueueArray), processor0(AUDIO_SAMPLE_RATE_EXACT), processor1(AUDIO_SAMPLE_RATE_EXACT) { }
    void update(void)
    {
        audio_block_t *block0 = receiveWritable(0);
        audio_block_t *block1 = receiveWritable(1);
        if (!block0 || !block1)
        {
            if (block0) release(block0);
            if (block1) release(block1);
            return;
        }

        CLIPPING = false;
        for (auto sampleIndex = 0; sampleIndex < AUDIO_BLOCK_SAMPLES; sampleIndex++)
        {
            auto sample0 = (double)block0->data[sampleIndex];
            auto sample1 = (double)block1->data[sampleIndex];

            sample0 = processor0.Process(sample0 / 32768.0, AUDIO_SAMPLE_RATE_EXACT) * 32768.0;
            sample1 = processor1.Process(sample1 / 32768.0, AUDIO_SAMPLE_RATE_EXACT) * 32768.0;

            if (sample0 >= CLIPPING_THRESHOLD || -sample0 <= -CLIPPING_THRESHOLD ||
                    sample1 >= CLIPPING_THRESHOLD || -sample1 <= -CLIPPING_THRESHOLD) CLIPPING = true;
            block0->data[sampleIndex] = (int)sample0;
            block1->data[sampleIndex] = (int)sample1;
        }

        if (CLIPPING)
            digitalWrite(ledPin, HIGH);
        else
            digitalWrite(ledPin, LOW);
        transmit(block0, 0);
        transmit(block1, 1);
        release(block0);
        release(block1);
    }

private:
    MonoSampleProcessor processor0;
    MonoSampleProcessor processor1;
    audio_block_t *inputQueueArray[2];
};


AudioInputI2S i2s2;
AudioEffectTeensyCar teensyCar1;
AudioOutputI2S i2s1;
AudioConnection patchCord1(i2s2, 0, teensyCar1, 0);
AudioConnection patchCord2(i2s2, 1, teensyCar1, 1);
AudioConnection patchCord3(teensyCar1, 0, i2s1, 0);
AudioConnection patchCord4(teensyCar1, 1, i2s1, 1);
AudioControlSGTL5000 audioShield;
void setup()
{
    pinMode(4, INPUT_PULLUP);
    AudioMemory(40);
    audioShield.enable();

    // Adjust the line level output voltage range. The following settings are possible:
    // 13: 3.16 Volts p-p  (what I give the woofers)
    // 14: 2.98 Volts p-p
    // 15: 2.83 Volts p-p
    // 16: 2.67 Volts p-p
    // 17: 2.53 Volts p-p
    // 18: 2.39 Volts p-p
    // 19: 2.26 Volts p-p
    // 20: 2.14 Volts p-p
    // 21: 2.02 Volts p-p
    // 22: 1.91 Volts p-p
    // 23: 1.80 Volts p-p
    // 24: 1.71 Volts p-p
    // 25: 1.62 Volts p-p
    // 26: 1.53 Volts p-p
    // 27: 1.44 Volts p-p
    // 28: 1.37 Volts p-p
    // 29: 1.29 Volts p-p  (default)
    // 30: 1.22 Volts p-p
    // 31: 1.16 Volts p-p
#if defined front_speaks || defined rear_speaks
    audioShield.lineOutLevel(13);
#endif
#ifdef woofers
    audioShield.lineOutLevel(13);
#endif

    // Adjust the sensitivity of the line-level inputs. Fifteen settings are possible:
    // 
    //  0: 3.12 Volts p-p
    //  1: 2.63 Volts p-p
    //  2: 2.22 Volts p-p  (pretty hot, Alpine stereo)
    //  3: 1.87 Volts p-p
    //  4: 1.58 Volts p-p
    //  5: 1.33 Volts p-p  (default)
    //  6: 1.11 Volts p-p
    //  7: 0.94 Volts p-p
    //  8: 0.79 Volts p-p
    //  9: 0.67 Volts p-p
    // 10: 0.56 Volts p-p
    // 11: 0.48 Volts p-p
    // 12: 0.40 Volts p-p
    // 13: 0.34 Volts p-p
    // 14: 0.29 Volts p-p
    // 15: 0.24 Volts p-p
#if defined front_speaks || defined rear_speaks
    audioShield.lineInLevel(0);
#endif
#ifdef woofers
    audioShield.lineInLevel(0);
#endif
}

void loop()
{
    if (digitalRead(4) == LOW) {
        QUIET = false;
    }
    else {
        QUIET = true;
    }
}
